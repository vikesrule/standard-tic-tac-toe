def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, index):
    print_board(board)
    print(board[index], "has won")
    print("GAME OVER")
    exit()

def is_row_winner(board, row_number):
    index = (row_number - 1) * 3
    return board[index] == board[index + 1] and board[index + 1] == board[index + 2]

def is_column_winner(board, column_number):
    index = column_number - 1
    return board[index] == board[index + 3] and board[index + 3] == board[index + 6]

def is_diagonal_winner(board, diagonal_number):
    index = (diagonal_number - 1) * 2
    if diagonal_number == 1:
        last = 8
    else:
        last = 6
    return board[index] == board[4] and board[4] == board[last]

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board, 0)
    elif is_row_winner(board, 2):
        game_over(board, 3)
    elif is_row_winner(board, 3):
        game_over(board, 6)
    elif is_column_winner(board, 1):
        game_over(board, 0)
    elif is_column_winner(board, 2):
        game_over(board, 1)
    elif is_column_winner(board, 3):
        game_over(board, 2)
    elif is_diagonal_winner(board, 1):
        game_over(board, 0)
    elif is_diagonal_winner(board, 2):
        game_over(board, 2)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
